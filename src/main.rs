/**
* Copyright 2021 Pramod Dematagoda
* This file is part of DA EService Parser.
*
*    DA EService Parser is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    DA EService Parser is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with DA EService Parser.  If not, see <https://www.gnu.org/licenses/>.
*
**/
use dialoguer::{theme::ColorfulTheme, Select};
use orange_da_eservice::{navigator::DaServiceSearchResults, parser::DaSearchResult};
use structopt::StructOpt;

use crate::orange_da_eservice::{
    navigator::DaServiceNavigator,
    printer::{print_da_information, print_stages},
};

mod orange_da_eservice;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = get_validated_arguments()?;

    let search_results = match args.da_number {
        Some(da) => DaServiceNavigator::search_by_development_application(da).await?,
        None => {
            DaServiceNavigator::search_by_address(
                args.street_number.unwrap(),
                args.street_name.unwrap(),
            )
            .await?
        }
    };

    handle_search_results(search_results).await
}

async fn handle_search_results(
    search_results: DaServiceSearchResults,
) -> Result<(), Box<dyn std::error::Error>> {
    let number_of_search_results = search_results.search_results.len();
    println!("Found {} matching results", number_of_search_results);

    if number_of_search_results == 0 {
        println!("No results, so exitting, try again with different parameters maybe?");
        return Ok(());
    }

    let result_number_to_fetch = ask_user_for_which_search_result(&search_results.search_results);

    let result_to_fetch = search_results
        .search_results
        .get(result_number_to_fetch)
        .unwrap()
        .to_owned();
    println!("Fetching {}", result_to_fetch.address);

    let result = search_results.fetch_result(result_to_fetch).await?;

    println!("\n\n==== Application Information ====\n");
    print_da_information(&result.result.application_information);

    println!("\n==== Application Stages & Progress ====\n");
    print_stages(result.result);

    Ok(())
}

#[derive(StructOpt, Debug)]
struct DaArguments {
    #[structopt(long)]
    pub street_number: Option<String>,

    #[structopt(long)]
    pub street_name: Option<String>,

    #[structopt(long)]
    pub da_number: Option<String>,
}

fn get_validated_arguments() -> Result<DaArguments, String> {
    let args = DaArguments::from_args();

    if args.da_number.is_none() {
        if args.street_name.is_none() || args.street_number.is_none() {
            return Err("Invalid arguments".to_string());
        }
    }

    Ok(args)
}

fn ask_user_for_which_search_result(search_results: &Vec<DaSearchResult>) -> usize {
    if search_results.len() == 1 {
        return 0;
    }

    let selections: Vec<String> = search_results
        .iter()
        .map(|search_result| search_result.address.to_owned())
        .collect();

    Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Select the address to view")
        .default(0)
        .items(&selections[..])
        .interact()
        .unwrap()
}

/**
* Copyright 2021 Pramod Dematagoda
* This file is part of DA EService Parser.
*
*    DA EService Parser is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    DA EService Parser is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with DA EService Parser.  If not, see <https://www.gnu.org/licenses/>.
*
**/
use select::{document::Document, node::Node, predicate::Name};

use std::collections::HashMap;

#[derive(Clone)]
pub struct DaSearchResult {
    pub link: String,
    pub address: String,
}

pub fn extract_search_results(results_blob: String) -> Vec<DaSearchResult> {
    let document = Document::from(results_blob.as_str());

    let matching_results = document
        .find(Name("a"))
        .filter(|link| is_link_match(link))
        .map(|result| extract_search_result(result))
        .collect();

    matching_results
}

fn is_link_match(link: &Node) -> bool {
    let is_right_class = link.attr("class").unwrap_or_default() == "plain_header";
    let link_href = link.attr("href").unwrap_or_default();
    let is_right_link = link_href.starts_with("/eservice/daEnquiryDetails.do");

    is_right_class && is_right_link
}

fn extract_search_result(link: Node) -> DaSearchResult {
    let search_result_link = link.attr("href").unwrap_or_default();
    let search_result_text = link.text();

    DaSearchResult {
        address: search_result_text,
        link: search_result_link.to_string(),
    }
}

pub struct DaStageProgress {
    pub current_stages: Vec<DaStage>,
    pub application_information: HashMap<String, String>,
}

pub struct DaStage {
    pub name: Option<String>,
    pub description: String,
    pub started: String,
    pub target: String,
    pub completed: String,
    pub status: String,
}

pub fn extract_da_information(result_blob: &String) -> DaStageProgress {
    let document = Document::from(result_blob.as_str());

    DaStageProgress {
        current_stages: extract_stages(result_blob),
        application_information: extract_application_information(&document),
    }
}

fn extract_application_information(result_document: &Document) -> HashMap<String, String> {
    let information_rows = result_document
        .find(Name("p"))
        .filter(|node| node.attr("class").unwrap_or_default() == "rowDataOnly");

    information_rows
        .filter(|row| row.first_child().unwrap().text().trim().len() != 0)
        .map(|row| {
            (
                row.first_child().unwrap().text(),
                row.last_child().unwrap().text(),
            )
        })
        .collect()
}

fn extract_stages(result_blob: &String) -> Vec<DaStage> {
    let document = Document::from(result_blob.as_str());

    let table = document
        .find(Name("table"))
        .filter(|table| {
            table.attr("summary").unwrap_or_default()
                == "Tasks Associated this Development Application"
        })
        .next()
        .unwrap();

    let relevant_rows = table
        .find(Name("tr"))
        .filter(|row| row.attr("class").unwrap_or_default() != "sub-heading")
        .map(|row| extract_stage(&row))
        .collect();

    relevant_rows
}

fn extract_stage(row: &Node) -> DaStage {
    let cells: Vec<Node> = row.find(Name("td")).collect();

    DaStage {
        name: Some(cells[0].text()),
        description: cells[1].text(),
        started: cells[2].text(),
        target: cells[3].text(),
        completed: cells[4].text(),
        status: cells[5].text(),
    }
}

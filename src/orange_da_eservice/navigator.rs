/**
* Copyright 2021 Pramod Dematagoda
* This file is part of DA EService Parser.
*
*    DA EService Parser is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    DA EService Parser is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with DA EService Parser.  If not, see <https://www.gnu.org/licenses/>.
*
**/
use reqwest::Client;
use urlencoding::encode;

use crate::orange_da_eservice::parser::extract_search_results;

use super::parser::{extract_da_information, DaSearchResult, DaStageProgress};

pub struct DaServiceNavigator {
    client: Client,
}

impl DaServiceNavigator {
    fn new() -> DaServiceNavigator {
        DaServiceNavigator {
            client: Client::builder().cookie_store(true).build().unwrap(),
        }
    }

    pub async fn search_by_address(
        street_number: String,
        street_name: String,
    ) -> Result<DaServiceSearchResults, Box<dyn std::error::Error>> {
        println!("Fetching for {} {}", street_number, street_name);

        perform_search(format!(
            "eservice/daEnquiry.do?streetName={}&suburb=1&houseNum={}&searchMode=A&submitButton=Search",
            street_name,
            street_number
        ))
        .await
    }

    pub async fn search_by_development_application(
        da: String,
    ) -> Result<DaServiceSearchResults, Box<dyn std::error::Error>> {
        println!("Fetching DA {}", da);

        perform_search(format!(
            "eservice/daEnquiry.do?number={}&searchMode=A&submitButton=Search",
            encode(da.as_str())
        ))
        .await
    }
}

async fn perform_search(
    search_url: String,
) -> Result<DaServiceSearchResults, Box<dyn std::error::Error>> {
    let navigator = DaServiceNavigator::new();

    initialise_server_state(&navigator.client).await?;

    let resp = navigator
        .client
        .get(get_url_with_base(search_url.as_str()))
        .send()
        .await?;

    let search_results = extract_search_results(resp.text().await?);

    Ok(DaServiceSearchResults {
        client: navigator.client,
        search_results,
    })
}

async fn initialise_server_state(client: &Client) -> Result<(), Box<dyn std::error::Error>> {
    println!("Performing initial request for cookie.");

    let _setup_response = client
        .get(get_url_with_base("eservice/daEnquiryInit.do?nodeNum=24"))
        .send()
        .await?;

    Ok(())
}

fn get_url_with_base(url: &str) -> String {
    "https://ecouncil.orange.nsw.gov.au/".to_string() + url
}

#[derive(Clone)]
pub struct DaServiceSearchResults {
    client: Client,
    pub search_results: Vec<DaSearchResult>,
}

impl DaServiceSearchResults {
    pub async fn fetch_result(
        self,
        result_to_fetch: DaSearchResult,
    ) -> Result<DaServiceStagesResult, Box<dyn std::error::Error>> {
        let da_result = self
            .client
            .get(get_url_with_base(result_to_fetch.link.as_str()))
            .send()
            .await?;

        let da_status_text = da_result.text().await?;

        let stages = extract_da_information(&da_status_text);

        Ok(DaServiceStagesResult { result: stages })
    }
}

pub struct DaServiceStagesResult {
    pub result: DaStageProgress,
}

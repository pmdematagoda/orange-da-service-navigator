/**
* Copyright 2021 Pramod Dematagoda
* This file is part of DA EService Parser.
*
*    DA EService Parser is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    DA EService Parser is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with DA EService Parser.  If not, see <https://www.gnu.org/licenses/>.
*
**/
use super::parser::DaStageProgress;

use std::collections::HashMap;

use cli_table::{print_stdout, Cell, CellStruct, Style, Table};

pub fn print_stages(da_stages: DaStageProgress) {
    let mut table: Vec<Vec<CellStruct>> = Vec::new();

    for stage in da_stages.current_stages {
        let table_row = vec![
            stage.name.unwrap_or_else(|| "-".to_string()).cell(),
            stage.description.cell(),
            stage.status.cell(),
            stage.started.cell(),
            stage.completed.cell(),
            stage.target.cell(),
        ];

        table.push(table_row);
    }

    let cli_table = table
        .table()
        .title(vec![
            "Name".cell(),
            "Description".cell(),
            "Status".cell(),
            "Started".cell(),
            "Completed".cell(),
            "Target".cell(),
        ])
        .bold(true);

    print_stdout(cli_table).unwrap();
}

pub fn print_da_information(information_map: &HashMap<String, String>) {
    let mut table: Vec<Vec<CellStruct>> = Vec::new();

    for entry in information_map.into_iter() {
        table.push(vec![entry.0.cell(), entry.1.cell()]);
    }

    let cli_table = table.table().bold(true);

    print_stdout(cli_table).unwrap();
}

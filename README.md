## Orange Council DA EService navigator

A very simple CLI application to interact with the Orange NSW Council portal for Development Applications. You may find the website easier to interact with, but this application lets you retrieve the status for a given DA application in a matter of seconds and in a more readable format (at least I think so).

## Disclaimers

This application and its author is not in anyway affiliated with the Orange NSW Council or the administrator of the service that is used.

While I have tested this application, at the end of the day it is just a glorified web scraper, I would recommend that you double check the results at least once before relying on this tool to retrieve DA applications.

## How do you use it?

Clone the repo and run `cargo run -- --help` to find out what arguments are applicable. I might publish a binary if there's a demand for it.